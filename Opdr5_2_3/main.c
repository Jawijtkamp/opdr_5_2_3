#include <msp430.h> 

#define ROOD  BIT0
#define BLAUW BIT1
#define GROEN BIT2

#define SW0 BIT0
#define SW1 BIT1

typedef enum {op_slot, dicht, open} toestand;

int main(void)
{
	WDTCTL = WDTPW | WDTHOLD;	                    // stop watchdog timer
	P1DIR = 0xFF;                                   // Poort 1 is output
	P1OUT = 0x00;                                   // Poort 1 Output = 0 (alles uit)
	P2DIR &= ~(0xFF);                               // Poort 2 is input
	P2REN |= 0xFF;                                  // Poort 2 interne weerstanden aan
	P2OUT &= ~(0xFF);                               // Poort 2 interne weerstanden op pulldown

	toestand deur = op_slot;                        // sluit de deur
    int sw0_huidig = 0;                             // variabele SW0 als knop 1 aangemaakt          huidige waarde knop 0
    int sw0_oud = 0;                                // variable SW0_oud voor oude waarde knop 1     oude waarde    knop 0
    int sw0_ingedrukt = 0;                          // Switch0 aangemaakt voor idk
    int sw1_huidig = 0;                             // variabele SW1 als knop 2 aangemaakt          huidige waarde knop 1
    int sw1_oud = 0;                                // variable SW0_oud voor oude waarde knop 2     oude waarde    knop 1
    int sw1_ingedrukt = 0;                          // Switch0 aangemaakt voor idk
	P1OUT &= ~(GROEN | BLAUW | ROOD);

	while (1)
	{
	    sw0_huidig = (P1IN & SW0);                 // P1.0 is gekoppeld aan SW0
	    sw1_huidig = (P1IN & SW1);                 // P1.1 is gekoppeld aan SW1

	    if (sw0_huidig == 0 && sw0_oud != 0)        // flankdetectie knop 0
	    {
	        sw0_ingedrukt = 1;                      // bruikbaar voor toestandsmachine
	    }

        if (sw1_huidig == 0 && sw1_oud != 0)        // flankdetectie knop 1
        {
            sw1_ingedrukt = 1;                      // bruikbaar voor toestandsmachine
        }

        switch (deur)                               // toestandsmachine begin
        {
        case op_slot:                               // de deur is huidig op slot
            P1OUT = (P1OUT & ~0x07) | GROEN;
            if (sw0_ingedrukt)
            {
                deur = dicht;
            }
            break;
        case dicht:                                 // de deur is dicht
            P1OUT = (P1OUT & ~0x07) | ROOD;
            if (sw1_ingedrukt)
            {
                deur = open;
            }
            else if (sw0_ingedrukt)
            {
                deur = op_slot;
            }
            break;
        case open:                                  // de deur is open
            P1OUT = (P1OUT & ~0x07) | BLAUW;
            if (sw1_ingedrukt)
            {
                deur = open;
            }
            break;
        }

        sw0_oud = sw0_huidig;
        sw0_ingedrukt = 0;
        sw1_oud = sw1_huidig;
        sw1_ingedrukt = 0;

        __delay_cycles(20 * 1100);                  // ongeveer 20ms wachten
    }
}
